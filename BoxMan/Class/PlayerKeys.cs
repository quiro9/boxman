﻿/*
 	Boxman is develop for Juan Quiroga (quiro90) <https://github.com/quiro90>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BoxMan
{
	/// <summary>
	/// Clase Entrada Jugador: maneja la interaccion del usuario con el jugador.
	/// intenta que responda teclado y pantalla tactil por igual.
	/// </summary>
	public class PlayerKeys
	{
		// keyboardJump indica si se preciono el boton de salto: 0 no, 1 si.
		// statusP es una variable temporal para guardar la pocición del jugador
		// jumpX es una bandera que indica que tecla se preciono al realizar el salto
		// de esta forma limitar el movimiento: 0 ninugna, 1 izquierda 2 derecha
		// waitReset: contador que se usa al "resetear"
		public int keyboardJump, statusP, jumpX, waitReset = 0;

		// keyboard state vars
		KeyboardState keyboard, keyboardOld;

		public PlayerKeys ()
		{
			keyboardJump = 0;
			statusP = 0;
			jumpX = 0;
		}

		public void Get (BoxMan game, Player player, Level level)
		{
			// -- CONTROLES --
			// añadimos keyboard como estado de tecla
			keyboard = Keyboard.GetState();

			// salida para iOS
			#if !__IOS__ &&  !__TVOS__
			if (GamePad.GetState (PlayerIndex.One).Buttons.Back == ButtonState.Pressed || keyboard.IsKeyDown (Keys.Escape))
				game.Exit ();
			#endif

			// se reinicia nivel (fuerzo reseteo)
			if (keyboard.IsKeyDown (Keys.R) && level.levelWin == 0) {
				// indicamos perdido
				player.playerAction = "ND";
				level.levelWin = -1;
				// detengo movimientos
				jumpX = 0;
				keyboardJump = 0;
			}
			// el inicio (se habilitan controles)
			else if (level.levelWin == 0) 
			{
				// Caida de player (eje y)
				if (level.colitionY == 1) {
					player.playerMoveY = 0;
					// al colisionar con el suelo "reincia salto y direccion de salto (jumpX)
					if (keyboardJump == 2) {
						keyboardJump = 0;
						jumpX = 0;
					}
				} else if (level.colitionY == 0) { // si esta en el aire cae...
					player.playerMoveY = 6;
				}
					
				// movimiento de barra ( eje X ) 
				// se preciona izquierda, controla que no colisione
				if (keyboard.IsKeyDown (Keys.Left) && level.colitionX != 1) {
					player.playerAction = "NL";
					// si salta y se preciona tecla opuesta, reduce velocidad
					if ((jumpX == 2 || jumpX == 4) && level.colitionX != 2)
						player.playerMoveX = 1;
					// si salta sin precionar tecla o si esta en caida libre se mueve "lento"
					else if ((jumpX == 0 || jumpX == 3) && level.colitionY == 0) {
						jumpX = 3;
						player.playerMoveX = -3;
					}
					// si salta para el lado izquierdo avanza o
					// si no salta avanza normalmente
					else if (jumpX == 1 || (level.colitionY == 1 && jumpX == 0)) {
						player.playerMoveX = -6;
						jumpX = 1;
					}
					// en caso de ser falso algun anterior no avanza (evita traspaso de paredes)
					else
						player.playerMoveX = 0;
				}
				// se preciona derecha (funciona de forma similar a izquierda)
				else if (keyboard.IsKeyDown (Keys.Right) && level.colitionX != 2) {
						player.playerAction = "NR";
					if ((jumpX == 1 || jumpX == 3) && level.colitionX != 1)
						player.playerMoveX = -1;
					else if ((jumpX == 0 || jumpX == 4) && level.colitionY == 0) {
						jumpX = 4;
						player.playerMoveX = 3;
					} 
					else if (jumpX == 2 || (level.colitionY == 1 && jumpX == 0)) {
						player.playerMoveX = 6;
						jumpX = 2;
					}
					else
						player.playerMoveX = 0;
				}
				// si no se preciona ni derecha ni izquierda
				else {
					// se toma en cuenta las posibilidades modificadas en el salto (no frena pero disminuye trayectoria)
					if ( keyboardJump != 0 && level.colitionX == 0) {
						if (jumpX == 1 || jumpX == 3) // se salto para la izquierda 
						player.playerMoveX = -2;
						if (jumpX == 2 || jumpX == 4) // se salto para la derecha
						player.playerMoveX = 2;
					} else {
						// si no se salto y no se preciona nada el jugador esta quieto
						player.playerMoveX = 0;
					} 
				}


				// se preciona boton de salto (tiene que tener suelo y no estar saltando)
				if (level.colitionY == 1 && keyboardJump == 0
				   && keyboard.IsKeyDown (Keys.Space)
				   && keyboard != keyboardOld) 
				{
					level.efectos [4].Play ();

					// precion de la barra, tomamos poción actual del juegador en eje Y
					// y cambiamos estado "keyboardJump" a 1
					statusP = player.playerRectangle.Location.Y;
					keyboardJump = 1;
					if (keyboard.IsKeyDown (Keys.Left)) {
						jumpX = 1;
					} else if (keyboard.IsKeyDown (Keys.Right)) {
						jumpX = 2;
					} else {
						jumpX = 0;
					}
				}

				// mientras el estado es 1 controlas que el Jugador este y no tenga colisión
				if (keyboardJump == 1 && level.colitionY != 2
					&& player.playerRectangle.Location.Y >= statusP - 110 ) {
					player.playerMoveY = -8;
				} 
				else if ( keyboardJump == 1 && level.colitionY != 2
					&& keyboard.IsKeyDown (Keys.Space) && player.playerRectangle.Location.Y >= statusP - 165) {
					player.playerMoveY = -8;

				} else { // si tiene colision o toco suelo, cambiamos estado de salto
					keyboardJump = 2;
				}

			}
			// el juego aun no inicia (espera precionar barra)
			else
			{
				if (waitReset > 10000) { //espera 1 segundo
					// reseteo del juego (cuando pasa de nivel o gana)
					if ((keyboard.IsKeyDown (Keys.Space) && keyboard != keyboardOld)
					    && (!keyboard.IsKeyDown (Keys.Left) && !keyboard.IsKeyDown (Keys.Right))) {
						// indica que comenzo juego y reinicia valores player
						level.levelWin = 0;
						game.gameReload = 1;
						level.levelScore = 0;
						player.playerType = 0;
						player.playerAction = "NR";
						jumpX = 0;
						keyboardJump = 0;
						waitReset = 0;
						player.playerChange = true;
					}
				} else {
					waitReset += game.gameCont;
				}
			}
	// guardamos estado antiguo para control de repeticion de teclas
			keyboardOld = keyboard;
			// -- FIN CONTROLES --

		}

	}
}

