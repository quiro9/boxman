﻿/*
 	Boxman is develop for Juan Quiroga (quiro9) <https://github.com/quiro9>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace BoxMan
{
	public class Camera
	{
		Viewport view;

		public Vector2 Position;
		public Matrix transform;
		public float minZoom, zoom, maxZoom;
		public float targetX, targetY;

		// -- camera class init _viewport; and take sizwW and sizeH (screenSize) --
		public Camera (Viewport _view)
		{
			view = _view;

			// init vars
			minZoom = 1f;
			maxZoom = 2f;
			zoom = 1f;

			// posición que toma la camara
			targetX = 150f;
			targetY = 50f;

		}

		// en ocaciones es necesario mover solo un EJE de la camara
		public void TargetX ( float _targetX)
		{
			this.targetX = _targetX;
		}
		public void TargetY ( float _targetY)
		{
			this.targetY = _targetY;
		}

		// -- Actualiza la posición de la camara tomando de referencia al jugador --
		public void Update (BoxMan game)
		{
			Position = new Vector2 ( this.targetX, this.targetY );
			transform = Matrix.CreateScale (new Vector3 (zoom, zoom, 0))
				* Matrix.CreateTranslation (new Vector3 (-Position.X, -Position.Y, 0));
		}

	}
}

