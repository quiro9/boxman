﻿/*
 	Boxman is develop for Juan Quiroga (quiro90) <https://github.com/quiro90>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace BoxMan
{
	/// <summary>
	/// Clase principal Nivel: Es la encargada de cargar los niveles e interactuar con el jugador en El.
	/// Las funciones muestran el contenido "pasado" de cada nivel y controlar algunos aspectos de estos
	/// como banderas de colisiones para con el jugador.
	/// </summary>
	public class Level
	{
		// Call class Level
		private NoFinal nofinal;

		private Level00 level00;
		private Level01 level01;
		private Level02 level02;

		// -- sound Vars --
		Song backMusic;
		public SoundEffect [] efectos; //esto declara una lista de sonidos

		// cantidad de rectangulos a dibujar
		public int rectangleCant;
		// {tipo, positionX, PositionY, eje, min, max,  velocidad, Widhth, Height, band };
		// tipo: dibuja rectangulo del esenario o objetos;
		// tipo de 0 a 4 rectangulos: 0 negro, 1 azul, 2verde, 3rojo 4 amarillo.
		// de 5 a 9 pinceles: 5 negro, 6 azul, 7 verde, 8 rojo 9 amarillo.
		// de 10 a 29 espinas: 10 negro, 11 azul, 12 verde, 13rojo, 14 amarillo ...
		// de 30 a 34 monedas ... ( todo posee el mismo orden de colores C/5 valores)
		// de 35 a 39 llaves, 40 a 44 bloques llaves view, 45 a 49 bloques llaves hide
		// de 50 a 54 bandera "gano" ... 55 es un bloque del color del fondo puede ocultar objetos,
		// eje: indica movimiento 1 ejeX, 2 EjeY
		// min, max: indica valor minimo y maximo en el eje para que s emueva
		// velocidad: cantidad de pixeles
		//    para objetos estaticos colocar valores minX, maxX, minY,maxY, velocidad en 0
		// band: varia segun el tipo, en plataformas mobiles indica "arriba abajo" o "derha izquierda"
		public int[,] rectangleType;

		// Rectangulo y textura de player 
		public Rectangle [] levelRectangle;
		public Texture2D [] levelSprite;
		public Rectangle titleRectangle, menuRectangle;
		public Texture2D titleSprite, menuSprite;

		// levelStartX, levelStartY (indica donde empieza jugador)
		public int levelStartX, levelStartY;
		// pasa el valor de la colision con objetos
		public int colitionY,colitionX, colitionCorrection;
		// bandera cuando se toma llave 
		// si [0] es menor a la cantidad de rectangulos los recorre... [1] almacena valor de llave a revisar...
		int [] keyband;

		// mapLevel: indica el esenario actual;
		// levelWin: 0 en transcurso, 1 gano, -1 perdio;
		public int mapLevel, levelWin;
		// tamaño del nivel (los limites)
		public int levelMinX, levelMaxX, levelMinY, levelMaxY;
		// tiempo para esenario (en segundos)
		// score indica el "puntaje" por nivel...
		public int levelTime, levelScore, totalScore;

		public Level ()
		{
			// inicializamos efectos
			efectos = new SoundEffect[6];

			// estos valores son llamados desde otras Clases
			colitionY = 0;
			colitionX = 0;
			mapLevel = 0;
			levelStartX = 0;
			levelStartY = 0;

			// valores inciales (se modifican por nivel)
			levelMinX = 0; 
			levelMaxX = 0;
			levelMinY = 0;
			levelMaxY = 0;

			levelTime = 0;
			levelScore = 0;

			rectangleCant = 5;
			rectangleType = new int[ rectangleCant, 10];

		}


		/// <summary>
		/// Carga todas las texturas de nivel. 
		/// </summary>
		public void Load (BoxMan game)
		{
			
			// carga el fondo para menu "a pantalla completa"
			this.menuSprite = game.Content.Load<Texture2D>("_menu");

			// cargala imagen TITULO
			this.titleSprite = game.Content.Load<Texture2D>("_title");

			// se carga musica y la reproduce "repitiendo"
			//backMusic = game.Content.Load<Song>("sound/backMusic");
			MediaPlayer.IsRepeating = true;
			MediaPlayer.Volume = 0.8f;
			//MediaPlayer.Play (backMusic);
			// efecto de sonidos
			efectos[0] = game.Content.Load<SoundEffect> ("sound/loser");
			efectos[1] = game.Content.Load<SoundEffect> ("sound/coint");
			efectos[2] = game.Content.Load<SoundEffect> ("sound/paint");
			efectos[3] = game.Content.Load<SoundEffect> ("sound/key");
			efectos[4] = game.Content.Load<SoundEffect> ("sound/jump");
			efectos[5] = game.Content.Load<SoundEffect> ("sound/win");
			SoundEffect.MasterVolume = 0.6f;

			// se carga el contenido de nivel
			this.levelSprite = new Texture2D[56];
			this.levelSprite[0] = game.Content.Load<Texture2D>("map/black");
			this.levelSprite[1] = game.Content.Load<Texture2D>("map/blue");
			this.levelSprite[2] = game.Content.Load<Texture2D>("map/green");
			this.levelSprite[3] = game.Content.Load<Texture2D>("map/red");
			this.levelSprite[4] = game.Content.Load<Texture2D>("map/yellow");
			this.levelSprite[5] = game.Content.Load<Texture2D>("map/black_paint");
			this.levelSprite[6] = game.Content.Load<Texture2D>("map/blue_paint");
			this.levelSprite[7] = game.Content.Load<Texture2D>("map/green_paint");
			this.levelSprite[8] = game.Content.Load<Texture2D>("map/red_paint");
			this.levelSprite[9] = game.Content.Load<Texture2D>("map/yellow_paint");
			this.levelSprite[10] = game.Content.Load<Texture2D>("map/black_thornD");
			this.levelSprite[11] = game.Content.Load<Texture2D>("map/blue_thornD");
			this.levelSprite[12] = game.Content.Load<Texture2D>("map/green_thornD");
			this.levelSprite[13] = game.Content.Load<Texture2D>("map/red_thornD");
			this.levelSprite[14] = game.Content.Load<Texture2D>("map/yellow_thornD");
			this.levelSprite[15] = game.Content.Load<Texture2D>("map/black_thornU");
			this.levelSprite[16] = game.Content.Load<Texture2D>("map/blue_thornU");
			this.levelSprite[17] = game.Content.Load<Texture2D>("map/green_thornU");
			this.levelSprite[18] = game.Content.Load<Texture2D>("map/red_thornU");
			this.levelSprite[19] = game.Content.Load<Texture2D>("map/yellow_thornU");
			this.levelSprite[20] = game.Content.Load<Texture2D>("map/black_thornL");
			this.levelSprite[21] = game.Content.Load<Texture2D>("map/blue_thornL");
			this.levelSprite[22] = game.Content.Load<Texture2D>("map/green_thornL");
			this.levelSprite[23] = game.Content.Load<Texture2D>("map/red_thornL");
			this.levelSprite[24] = game.Content.Load<Texture2D>("map/yellow_thornR");
			this.levelSprite[25] = game.Content.Load<Texture2D>("map/black_thornR");
			this.levelSprite[26] = game.Content.Load<Texture2D>("map/blue_thornR");
			this.levelSprite[27] = game.Content.Load<Texture2D>("map/green_thornR");
			this.levelSprite[28] = game.Content.Load<Texture2D>("map/red_thornR");
			this.levelSprite[29] = game.Content.Load<Texture2D>("map/yellow_thornR");
			this.levelSprite[30] = game.Content.Load<Texture2D>("map/black_coint");
			this.levelSprite[31] = game.Content.Load<Texture2D>("map/blue_coint");
			this.levelSprite[32] = game.Content.Load<Texture2D>("map/green_coint");
			this.levelSprite[33] = game.Content.Load<Texture2D>("map/red_coint");
			this.levelSprite[34] = game.Content.Load<Texture2D>("map/yellow_coint");

			this.levelSprite[35] = game.Content.Load<Texture2D>("map/black_key");
			this.levelSprite[36] = game.Content.Load<Texture2D>("map/blue_key");
			this.levelSprite[37] = game.Content.Load<Texture2D>("map/green_key");
			this.levelSprite[38] = game.Content.Load<Texture2D>("map/red_key");
			this.levelSprite[39] = game.Content.Load<Texture2D>("map/yellow_key");
			this.levelSprite[40] = game.Content.Load<Texture2D>("map/black_kview");
			this.levelSprite[41] = game.Content.Load<Texture2D>("map/blue_kview");
			this.levelSprite[42] = game.Content.Load<Texture2D>("map/green_kview");
			this.levelSprite[43] = game.Content.Load<Texture2D>("map/red_kview");
			this.levelSprite[44] = game.Content.Load<Texture2D>("map/yellow_kview");
			this.levelSprite[45] = game.Content.Load<Texture2D>("map/black_khide");
			this.levelSprite[46] = game.Content.Load<Texture2D>("map/blue_khide");
			this.levelSprite[47] = game.Content.Load<Texture2D>("map/green_khide");
			this.levelSprite[48] = game.Content.Load<Texture2D>("map/red_khide");
			this.levelSprite[49] = game.Content.Load<Texture2D>("map/yellow_khide");

			this.levelSprite[50] = game.Content.Load<Texture2D>("map/black_win");
			this.levelSprite[51] = game.Content.Load<Texture2D>("map/blue_win");
			this.levelSprite[52] = game.Content.Load<Texture2D>("map/green_win");
			this.levelSprite[53] = game.Content.Load<Texture2D>("map/red_win");
			this.levelSprite[54] = game.Content.Load<Texture2D>("map/yellow_win");

			this.levelSprite[55] = game.Content.Load<Texture2D>("map/_white");

		}

		public void ReLoad(Enemy enemy, int scrW = 0, int scrH = 0)
		{
			// cada vez que recarga el nivel "borra" enemigos
			enemy.enemyCant = 0;

			// Internaliza las Clases de niveles segun el dato pasado
			switch (this.mapLevel)
			{	
			case 0:
				// nivel 0 (tutorial basico)
				//this.levelStartX = scrW / 2;
				//this.levelStartY = scrH - (scrH /3);
				this.levelStartX = 250;
				this.levelStartY = 450;
				level00 = new Level00(this);
				break;
			case 1:
				// carga nivel 1
				this.levelStartX = 225;
				this.levelStartY = 490;
				level01 = new Level01(this,enemy);
				break;
			case 2:
				// carga nivel 2
				this.levelStartX = 100;
				this.levelStartY = 450;
				level02 = new Level02(this,enemy);
				break;
			default:
				// Fin!
				this.levelStartX = 100;
				this.levelStartY = 300;
				nofinal = new NoFinal(this,enemy);

				break;

			}

			// coloca la bandera llave 
			keyband = new int[2]; 
			keyband [0] = rectangleCant+1;

			//luego de cargar valores reposiciona los rectangulos en el esenario
			this.levelRectangle = new Rectangle [this.rectangleCant];
			for (int i = 0; i < this.rectangleCant; i++)
			{
				this.levelRectangle[i] = new Rectangle( rectangleType[i,1], rectangleType[i,2], rectangleType[i,7], rectangleType[i,8]);
			}	
		}


		/// <summary>
		/// "dibujado" del nivel, en este bucle se examina algunas cosas como colisiones.
		/// </summary>
		public void Draw (SpriteBatch spriteBatch, Color gameColor)
		{
			for (int i = 0; i < this.rectangleCant; i++) {
				// dibuja el nivel (si no esta borrado)
				if (!this.levelSprite [this.rectangleType [i, 0]].IsDisposed) {
					// dibuja la textura actual
					spriteBatch.Draw (levelSprite [rectangleType [i, 0]], levelRectangle [i], gameColor);

				}
			}
		}

		/// <summary>
		/// chequea la colisiones de player y enemigo con esenario
		/// tambien controla las banderas "perdio" y siguiente nivel
		/// </summary>
		public void Update(Player player, int gameCont)
		{
			// aprovechando el bucle del nivel, se controlan todos los objetos y posibles estados
			this.colitionX = 0;
			this.colitionY = 0;
			colitionCorrection = 0;

			for (int i = 0; i < this.rectangleCant; i++) 
			{
				// si coliciona con objeto
				if (player.playerRectangle.Intersects (this.levelRectangle [i])) 
				{
					colitionCorrection = colitionY;
					// colisiones con todos los cuadrados negros y segun el "tipo" de color de cuadrado
					if ( (this.rectangleType [i, 0] == 0 || this.rectangleType[i, 0] == 40) || 
						(player.playerType == this.rectangleType [i, 0] || player.playerType == this.rectangleType [i, 0]-40) &&
						(this.rectangleType [i, 0] <= 4 || (this.rectangleType [i, 0] >= 40 && this.rectangleType [i, 0] <= 44) )) {
						// controlamos las coliciones de player
						if (player.playerRectangle.Location.Y + player.playerHeight <= this.levelRectangle [i].Location.Y + 10) {
							// si coliciona con el suelo
							this.colitionY = 1;
						} else if (player.playerRectangle.Location.Y >= (this.levelRectangle [i].Location.Y + this.levelRectangle [i].Height - 10)) {
							// si colisiona con techo
							this.colitionY = 2;
						} else if (player.playerRectangle.Location.X >= this.levelRectangle [i].Location.X + (this.levelRectangle [i].Width - 10)) {
							// si tiene alguna resta en X
							this.colitionX = 1;
						} else if (player.playerRectangle.Location.X + player.playerWidth <= this.levelRectangle [i].Location.X + 10) {
							// si tiene alguna resta en X
							this.colitionX = 2;
						} 
						// correccion de colisiones (si toca suelo y cabeza)
						if ((this.colitionY == 2 && colitionCorrection == 1) || 
							this.colitionY == 1 && colitionCorrection == 1)
							this.colitionY = 1;
							

					}


					// controlamoslos objetos "pinceles" (del 5 al 9)
					else if (this.rectangleType [i, 0] >= 5 && this.rectangleType [i, 0] <= 9 && !this.levelRectangle [i].IsEmpty) {
						// se compara que el cuerpo del jugador este 1/4 sobre el pincel
						if (player.playerRectangle.Y + player.playerHeight / 6 <= this.levelRectangle [i].Location.Y + this.levelRectangle [i].Height &&
						    (player.playerRectangle.X + (player.playerWidth - player.playerWidth / 6) > this.levelRectangle [i].Location.X &&
						    player.playerRectangle.X + player.playerWidth / 6 < this.levelRectangle [i].Location.X + this.levelRectangle [i].Width)) {

							// desdibuja el rectangulo (IsEmpty) toma la propiedad cuando su tamaño es 0 y se encuentra en el punto 0,0 de la pantalla
							this.levelRectangle [i].Location = new Point (0, 0);
							this.levelRectangle [i].Width = 0;
							this.levelRectangle [i].Height = 0;

							// suma puntaje por cada pincel "tomado" correspondiente al color
							this.levelScore += 20 + (this.rectangleType [i, 0] - 5) * 4;
							// cambiamos el valor a que jugador cambio de color
							player.playerType = this.rectangleType [i, 0] - 5;
							player.playerChange = true;

							// reproduce sonido
							efectos [2].Play ();
						}
					}
					

					// si colisiona con "espinas" negras o del mismo color perdes
					else if ((this.rectangleType [i, 0] >= 10 && this.rectangleType [i, 0] <= 29) &&
					         (this.rectangleType [i, 0] == 10 ||
					         this.rectangleType [i, 0] == 15 ||
					         this.rectangleType [i, 0] == 20 ||
					         this.rectangleType [i, 0] == 25 ||
					         this.rectangleType [i, 0] - 10 == player.playerType ||
					         this.rectangleType [i, 0] - 15 == player.playerType ||
					         this.rectangleType [i, 0] - 20 == player.playerType ||
					         this.rectangleType [i, 0] - 25 == player.playerType)) {
						if (player.playerRectangle.Y + player.playerHeight / 3 <= this.levelRectangle [i].Location.Y + this.levelRectangle [i].Height &&
							(player.playerRectangle.X + (player.playerWidth - player.playerWidth / 5) > this.levelRectangle [i].Location.X &&
							player.playerRectangle.X + player.playerWidth / 5 < this.levelRectangle [i].Location.X + this.levelRectangle [i].Width) )
							this.levelWin = -1;
					}


					// toma las monedas
					else if (this.rectangleType [i, 0] >= 30 && this.rectangleType [i, 0] <= 34 && !this.levelRectangle [i].IsEmpty) {
						// se compara que el cuerpo del jugador este 1/4 sobre el pincel
						if (player.playerRectangle.Y + player.playerHeight / 6 <= this.levelRectangle [i].Location.Y + this.levelRectangle [i].Height &&
						    (player.playerRectangle.X + (player.playerWidth - player.playerWidth / 6) > this.levelRectangle [i].Location.X &&
						    player.playerRectangle.X + player.playerWidth / 6 < this.levelRectangle [i].Location.X + this.levelRectangle [i].Width) &&
						    (this.rectangleType [i, 0] == 30 || this.rectangleType [i, 0] - 30 == player.playerType)) {
							// dibuja el rectangulo (IsEmpty) toma la propiedad cuando su tamaño es 0 y se encuentra en el punto 0,0 de la pantalla
							this.levelRectangle [i].Location = new Point (0, 0);
							this.levelRectangle [i].Width = 0;
							this.levelRectangle [i].Height = 0;

							// suma puntaje por cada moneda "tomada" correspondiente al color
							this.levelScore += 50 + (this.rectangleType [i, 0] - 30) * 2;

							// reproduce sonido
							efectos [1].Play ();
						}
					} 

					// toma llaves negras o del mismo color...
					else if (this.rectangleType [i, 0] >= 35 && this.rectangleType [i, 0] <= 39 && !this.levelRectangle [i].IsEmpty) 
					{
						// se compara que el cuerpo del jugador este 1/4 sobre el pincel
						if ( player.playerRectangle.Y + player.playerHeight / 6 <= this.levelRectangle [i].Location.Y + this.levelRectangle [i].Height &&
							(player.playerRectangle.X + (player.playerWidth - player.playerWidth / 6) > this.levelRectangle [i].Location.X &&
							 player.playerRectangle.X + player.playerWidth / 6 < this.levelRectangle [i].Location.X + this.levelRectangle [i].Width)&&
							(this.rectangleType [i, 0] == 35 || this.rectangleType [i, 0] - 35 == player.playerType))
						{

							// desdibuja el rectangulo (IsEmpty) toma la propiedad cuando su tamaño es 0 y se encuentra en el punto 0,0 de la pantalla
							this.levelRectangle [i].Location = new Point (0, 0);
							this.levelRectangle [i].Width = 0;
							this.levelRectangle [i].Height = 0;

							// da el valor d ela llave tomda (0 a 5) se guarda en el ultimo valor
							keyband[1] = this.rectangleType [i, 0] - 35;
							keyband[0] = 0; //indica que hay valor para revisar los loques a cambiar

							efectos [3].Play ();
						}
					}
						

					// si colisiona con "bandera gano!" negra o del mismo color.
					else if ( ( this.rectangleType [i, 0] >= 50 && this.rectangleType [i, 0] <= 54) &&
						    ( this.rectangleType [i, 0] == 50 || this.rectangleType [i, 0] - 50 == player.playerType 
							&& colitionY == 1 ) ) {
						this.levelWin = 1;
					}
				} // fin colision

				// -- plataformas de movimiento --
				// objeto en movimiento colision por color


				if (rectangleType [i, 3] == 0 && rectangleType [i, 6] > 0) { // se mueve a los lados
					if (levelRectangle [i].Location.X > rectangleType [i, 4] && rectangleType [i, 9] == 0) {
						levelRectangle [i].Location = new Point (this.levelRectangle [i].Location.X - rectangleType [i, 6], this.levelRectangle [i].Location.Y);
						if (player.playerRectangle.Intersects (levelRectangle [i]) && colitionX != 1 && 
							(this.rectangleType [i, 0] == 0 || this.rectangleType [i, 0] == 40  || 
							player.playerType == this.rectangleType [i, 0] || player.playerType == this.rectangleType [i, 0] - 40 ) &&
							( this.rectangleType [i, 0] <= 4 || (this.rectangleType [i, 0] >= 40 && this.rectangleType [i, 0] <= 44) )) {
							player.playerRectangle.Location = new Point (player.playerRectangle.Location.X - rectangleType [i, 6], player.playerRectangle.Location.Y);
						}
					} else if (levelRectangle [i].Location.X < rectangleType [i, 5] && rectangleType [i, 9] == 1) {
						levelRectangle [i].Location = new Point (this.levelRectangle [i].Location.X + rectangleType [i, 6], this.levelRectangle [i].Location.Y);
						if (player.playerRectangle.Intersects (levelRectangle [i]) && colitionX != 2 && 
							(this.rectangleType [i, 0] == 0 || this.rectangleType [i, 0] == 40  || 
							 player.playerType == this.rectangleType [i, 0] || player.playerType == this.rectangleType [i, 0] - 40 ) &&
							(this.rectangleType [i, 0] <= 4 || (this.rectangleType [i, 0] >= 40 && this.rectangleType [i, 0] <= 44) )) 
							{
								player.playerRectangle.Location = new Point (player.playerRectangle.Location.X + rectangleType [i, 6], player.playerRectangle.Location.Y);
							}
					} else {
						if (rectangleType [i, 9] == 1)
							rectangleType [i, 9] = 0;
						else
							rectangleType [i, 9] = 1;
					}
				} 
				else if (rectangleType [i, 3] == 1 && rectangleType [i, 6] > 0) { // se mueve arriba o abajo
					if (levelRectangle [i].Location.Y > rectangleType [i, 4] && rectangleType [i, 9] == 0) {
						levelRectangle [i].Location = new Point (this.levelRectangle [i].Location.X, this.levelRectangle [i].Location.Y - rectangleType [i, 6]);
						if (player.playerRectangle.Intersects (levelRectangle [i]) && colitionY != 2 && 
							(this.rectangleType [i, 0] == 0 || this.rectangleType [i, 0] == 40  || 
							player.playerType == this.rectangleType [i, 0] || player.playerType == this.rectangleType [i, 0] - 40 ) &&
							( this.rectangleType [i, 0] <= 4 || (this.rectangleType [i, 0] >= 40 && this.rectangleType [i, 0] <= 44) )) {
							player.playerRectangle.Location = new Point (player.playerRectangle.Location.X, player.playerRectangle.Location.Y - rectangleType [i, 6]);
						}
					} else if (levelRectangle [i].Location.Y < rectangleType [i, 5] && rectangleType [i, 9] == 1) {
						levelRectangle [i].Location = new Point (this.levelRectangle [i].Location.X, this.levelRectangle [i].Location.Y + rectangleType [i, 6]);
						if (player.playerRectangle.Intersects (levelRectangle [i]) && 
							(this.rectangleType [i, 0] == 0 || this.rectangleType [i, 0] == 40  || 
							player.playerType == this.rectangleType [i, 0] || player.playerType == this.rectangleType [i, 0] - 40 ) &&
							( this.rectangleType [i, 0] <= 4 || (this.rectangleType [i, 0] >= 40 && this.rectangleType [i, 0] <= 44) )) {
							player.playerRectangle.Location = new Point (player.playerRectangle.Location.X, player.playerRectangle.Location.Y + rectangleType [i, 6]);
						}
					} else {
						if (rectangleType [i, 9] == 1)
							rectangleType [i, 9] = 0;
						else
							rectangleType [i, 9] = 1;
					}

				}// -- fin de plataforma movimiento --


				// -- revisa todos los bloques de llaves a cambiar ---
				if (keyband[0] < rectangleCant) {
					if (rectangleType [i, 0] == 40 + keyband [1]) {
						rectangleType [i, 0] += 5;
					}
					else if (rectangleType [i, 0] == 45 + keyband [1]) {
						rectangleType [i, 0] -= 5;
					}

					// suma valor (nuevo rectangulo a revisar)
					keyband[0] += 1;	
				} // -- fin revisa bloqueas a cambiar --


			} // fin for


			// --- CONDICIONALES DE PERDER O GANAR ---
			// si jugador sale del esenario
			if ( player.playerRectangle.Location.Y > this.levelMaxY+310 || player.playerRectangle.Location.Y < this.levelMinY-310
				|| player.playerRectangle.Location.X > this.levelMaxX+300 || player.playerRectangle.Location.X < this.levelMinX-300) {
				this.levelWin = -1;
			}
			// tiempo en el juego resta cada 1 segundo
			if (this.levelWin == 0 && gameCont % 999 == 0) {
				this.levelTime -= 1;
				if (this.levelTime <= 0) {
					this.levelWin = -2;
				}
			}
			// cuando pierde
			if (this.levelWin != -3 && this.levelWin <= -1) {
				player.playerAction = "ND";
				efectos [0].Play ();
			// cuando gana
			} else if (this.levelWin != 2 && this.levelWin == 1) {
				this.levelScore += this.levelTime * 10;
				this.totalScore += this.levelScore;
				this.mapLevel += 1;
				efectos [5].Play ();
				levelWin = 2;
			}
			//reseteo de variables se realiza en la Clase Events ()

		}
			
	}

}

