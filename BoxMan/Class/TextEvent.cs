﻿/*
 	Boxman is develop for Juan Quiroga (quiro9) <https://github.com/quiro9>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoxMan
{
	/// <summary>
	/// Modifica o crea datos en el juego a medida que transcurre...
	/// los eventos pueden llamarse por colisiones o puntos X,Y que se encuentre el jugador
	/// Como eventos puede dibujar texto, se llama entre spriteBatch
	/// </summary>
	public class TextEvent
	{
		// -- variables de tiempo ---
		int timeMinutes = 0, timeSecond = 0;
		// indica si finalizo el juego...
		int endgame = 0;
		// --- bandera para eventos (se reinicia a cada nivel) ---
		int [] eventBand;
		int eventEnd = 1; //indica el inicio, se cambia a 0, luego hasta el final no se coloca denuevo en 1

		public TextEvent ()
		{
		}


		public void Control(BoxMan game, Player player, Level level, Camera camera)
		{

			// MUESTRA TIEMPO (exepto si el juego termino)
			if (endgame == 0) {
				if (level.levelTime > 60) {
					timeMinutes = level.levelTime / 60;
					timeSecond = level.levelTime - (60 * timeMinutes);
					game.spriteBatch.DrawString (game.fontSprite, "TIEMPO RESTANTE: " + timeMinutes.ToString () + "." + timeSecond.ToString (), new Vector2 (camera.targetX + (game.screenWidth / 2) - 140, camera.targetY + (game.screenHeight / 30)), Color.Orange);

				} else {
					if (level.levelTime < 15)
						game.spriteBatch.DrawString (game.fontSprite, "TIEMPO RESTANTE: 0." + level.levelTime.ToString (), new Vector2 (camera.targetX + (game.screenWidth / 2) - 140, camera.targetY + (game.screenHeight / 30)), Color.Red);
					else
						game.spriteBatch.DrawString (game.fontSprite, "TIEMPO RESTANTE: 0." + level.levelTime.ToString (), new Vector2 (camera.targetX + (game.screenWidth / 2) - 140, camera.targetY + (game.screenHeight / 30)), Color.Orange);
				}
			}
			// FIN DE MUESTRA TIEMPO

			if (level.levelWin == 0) {
				// Internaliza las Clases de niveles segun el dato pasado
				switch (level.mapLevel) {	
				case 0:
					game.spriteBatch.DrawString (game.fontSprite, "- Tutorial -", new Vector2 (150, game.screenHeight - 60), Color.White);
					if (eventEnd == 1) {
						eventBand = new int[4] { 1, 1, 1, 1 };
						eventEnd = 0;
					}		
					else if (player.playerRectangle.Location.X <= 400 && player.playerRectangle.Location.Y > 400 && eventBand [0] == 1) {
						game.spriteBatch.DrawString (game.fontSprite, "Caminar con Flechas: ", new Vector2 (camera.targetX / 10 - 5, game.screenHeight - 25), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "        Derecha \\ Izquierda", new Vector2 (camera.targetX / 10 - 5, game.screenHeight - 5), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "Salto: Barra Espaciadora", new Vector2 (camera.targetX / 10 - 5, game.screenHeight + 15), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "Reiniciar: R", new Vector2 (camera.targetX / 10 - 5, game.screenHeight + 35), Color.Orange);
					} else if (player.playerRectangle.Location.X > 450 && player.playerRectangle.Location.Y < 650 && eventBand [1] == 1) {
						eventBand [0] = 0;	
						game.spriteBatch.DrawString (game.fontSprite, "- Pinceles -", new Vector2 (camera.targetX / 3 + 580, game.screenHeight - 150), Color.White);
						game.spriteBatch.DrawString (game.fontSprite, "Siempre podes tocar lo negro,", new Vector2 (camera.targetX / 4 + 450, game.screenHeight - 120), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "pero a veces para avanzar o", new Vector2 (camera.targetX / 4 + 450, game.screenHeight - 100), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "tocar otros objetos, necesitas", new Vector2 (camera.targetX / 4 + 450, game.screenHeight - 80), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "cambiar tu color.", new Vector2 (camera.targetX / 4 + 450, game.screenHeight - 60), Color.Orange);
						game.spriteBatch.DrawString (game.fontSprite, "Agarra el pincel!", new Vector2 (camera.targetX / 4 + 450, game.screenHeight - 40), Color.DeepSkyBlue);
					} else if (player.playerRectangle.Location.X < 600 && player.playerRectangle.Location.Y < 200 && player.playerRectangle.Location.Y > 90 && eventBand [2] == 1) {
						eventBand [1] = 0;
						game.spriteBatch.DrawString (game.fontSprite, "Los pinceles y monedas Suman Puntos!", new Vector2 (camera.targetX / 10 - 30, 280), Color.White);
						game.spriteBatch.DrawString (game.fontSprite, "Toma todos los que puedas!", new Vector2 (camera.targetX / 10 - 30, 300), Color.White);
					} else if (player.playerRectangle.Location.Y < 100 && eventBand [3] == 1) {
						eventBand [2] = 0;
						game.spriteBatch.DrawString (game.fontSprite, "Los Pinceles solo se toman una vez!", new Vector2 (camera.targetX / 10 + 80, -50), Color.GreenYellow);
						game.spriteBatch.DrawString (game.fontSprite, "Si no queres quedar sin salida", new Vector2 (camera.targetX / 10 + 90, -20), Color.GreenYellow);
						game.spriteBatch.DrawString (game.fontSprite, "Evita Caer a zonas ya recorridas", new Vector2 (camera.targetX / 10 + 70, 110), Color.White);
					} 
					break;
				case 1:
					game.spriteBatch.DrawString (game.fontSprite, "- Nivel 1 -", new Vector2 (150, game.screenHeight - 60), Color.White);
					if (eventEnd == 1) {
						eventBand = new int[2] { 1, 1 };
						eventEnd = 0;
					} else if (player.playerRectangle.Location.X <= 550 && player.playerRectangle.Location.Y > 350 && eventBand [0] == 1) {
						game.spriteBatch.DrawString (game.fontSprite, "Evita esas cositas volando,", new Vector2 (camera.targetX / 4 + game.screenWidth / 12, game.screenHeight - 45), Color.White);
						game.spriteBatch.DrawString (game.fontSprite, "si te tocan perdes!", new Vector2 (camera.targetX / 4 + game.screenWidth / 12, game.screenHeight - 30), Color.White);
					} else {
						eventBand [0] = 0;
					}
					if (player.playerRectangle.Location.Y > 650 && eventBand [1] == 1){
						game.spriteBatch.DrawString (game.fontSprite, "Cuidado con las Espinas!", new Vector2 (camera.targetX / 12 - game.screenWidth / 9, game.screenHeight + 300), Color.Black);
					}
					break;
				case 2:
					game.spriteBatch.DrawString (game.fontSprite, "- Nivel 2 -", new Vector2 (150, game.screenHeight - 55), Color.White);
					if (eventEnd == 1) {
						eventBand = new int[3] { 1, 1, 1 };
						eventEnd = 0;
					}
					if (player.playerRectangle.Location.X > -50 && player.playerRectangle.Location.X < 700 && player.playerRectangle.Location.Y < 690 && eventBand [0] == 1) {
						game.spriteBatch.DrawString (game.fontSprite, "Las llaves cambian los bloques con sus dibujos", new Vector2 (camera.targetX / 8 +50, game.screenHeight - 35), Color.White);
						game.spriteBatch.DrawString (game.fontSprite, "y colores, agarrarlas en un orden correcto!", new Vector2 (camera.targetX / 8 +50, game.screenHeight - 15), Color.White);
					} else if (player.playerRectangle.Location.Y > 690 && player.playerRectangle.Location.X < 650 && eventBand [2] == 1) {
						eventBand [1] = 0;
						game.spriteBatch.DrawString (game.fontSprite, "Evita los cubis!", new Vector2 (camera.targetX / 16 + 370 - game.screenWidth, game.screenHeight + 620), Color.Black);
						game.spriteBatch.DrawString (game.fontSprite, "Son esas cositas cuadradas", new Vector2 (camera.targetX / 16 + 600 - game.screenWidth, game.screenHeight + 620), Color.Black);
					} else {
						eventBand [2] = 0;
					}
					break;
				default:
					endgame = 1;
					level.titleRectangle = new Rectangle ((int)camera.targetX / 2 + game.screenWidth / 12, (int)game.screenHeight / 2, game.screenWidth / 2, game.screenHeight / 10);
					game.spriteBatch.Draw (level.titleSprite, level.titleRectangle, Color.White);
					game.spriteBatch.DrawString (game.fontSprite, "- FIN? GRACIAS POR JUGAR! -", new Vector2 (camera.targetX / 2 + game.screenWidth / 8, game.screenHeight + 40), Color.White);
					game.spriteBatch.DrawString (game.fontSprite, "Total Ganado: " + level.totalScore.ToString (), new Vector2 (camera.targetX / 2 + game.screenWidth / 8, game.screenHeight + 60), Color.White);
					game.spriteBatch.DrawString (game.fontSprite, "Precione Esc Para Salir!", new Vector2 (camera.targetX / 2 + game.screenWidth / 8, game.screenHeight + 80), Color.White);

					break;
				}
			} 
			// MUESTRA MENSAJES CUANDO SE DETIENE EL JUEGO...
			else {
				// reseteo de variables:
				player.playerAnimNumber = 0;
				player.playerMoveX = 0;
				player.playerMoveY = 0;
				player.playerChange = true;

				eventEnd = 1; //reinicia bandera

				level.menuRectangle = new Rectangle ((int)camera.targetX, (int)camera.targetY, game.screenWidth, game.screenHeight);
				level.titleRectangle = new Rectangle ((int)camera.targetX+game.screenWidth/4, (int)camera.targetY+game.screenHeight/12, game.screenWidth/2, game.screenHeight/10);

				game.spriteBatch.Draw (level.menuSprite, level.menuRectangle, Color.White);
				game.spriteBatch.Draw (level.titleSprite, level.titleRectangle, Color.White);

				// si no inicia
				if (level.levelWin != -3 && level.levelWin != 9) {
					game.spriteBatch.DrawString (game.fontSprite, "Tu Puntaje: " + level.levelScore.ToString (), new Vector2 (camera.targetX + game.screenWidth / 2 - 180, camera.targetY + game.screenHeight / 3 + 35), Color.Blue);
					game.spriteBatch.DrawString (game.fontSprite, "Total Ganado: " + level.totalScore.ToString (), new Vector2 (camera.targetX + game.screenWidth / 2 - 180, camera.targetY + game.screenHeight / 3 + 60), Color.Blue);
				}

				game.spriteBatch.DrawString (game.fontSprite, "< Barra Espaciadora >", new Vector2 (camera.targetX+game.screenWidth/2 - 155, camera.targetY+(game.screenHeight-(game.screenHeight/3-32))), Color.Black);
				game.spriteBatch.DrawString (game.fontSprite, "[ Para Comenzar ]", new Vector2 (camera.targetX+game.screenWidth/2 - 120, camera.targetY+(game.screenHeight-(game.screenHeight/3))), Color.Black);

			}

			if (level.levelWin != -3 && level.levelWin <= -1) { // perdio
				eventEnd = 1;
				if (level.levelWin == -2 ) {
					game.spriteBatch.DrawString (game.fontSprite, "Se termino el tiempo! :( ", new Vector2 (camera.targetX + game.screenWidth / 2 - 180, camera.targetY + game.screenHeight / 5+20), Color.OrangeRed);
				} else {
					game.spriteBatch.DrawString (game.fontSprite, "Upsss... Ya Perdiste! :( ", new Vector2 (camera.targetX + game.screenWidth / 2 - 180, camera.targetY + game.screenHeight / 5+20), Color.OrangeRed);

				}
			} 
			else if (level.levelWin >= 1) { // gano
				if (level.levelWin != 9) {
					game.spriteBatch.DrawString (game.fontSprite, " Nivel Completo! :D ", new Vector2 (camera.targetX + game.screenWidth / 2 - 180, camera.targetY + game.screenHeight / 5 + 20), Color.OrangeRed);
				}
			}
			// fin detencion de juego




		}

	}
}

