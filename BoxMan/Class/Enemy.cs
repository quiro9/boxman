﻿/*
 	Boxman is develop for Juan Quiroga (quiro9) <https://github.com/quiro9>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BoxMan
{
	/// <summary>
	/// Enemy se utiliza solo para cargar contenido de enemy y leer las "configuraciones"
	/// dadas en level, el update y los rectangulos son llamado dentro de level
	/// </summary>
	public class Enemy
	{

		// --  Vars --
		// tipo de enmigo y pociciones
		// en centinellas
		// {tipo, posicionInicialX, pocicionInicialY,desplazamientoX, desplazamientoY, minimoX, MaximoX, minimoY, MaximoY, banderaX, banderaY}
		// en cannon (band se maeja utomaticamente al igual que timecont)
		// {tipo, posicionFijaX, pocicionFijaY,+-desplazamientoX, +-desplazamientoY, valorX, valorY, tiempoMS, null, 0 = band, 0 = timeCont}
		// en cubi (band 1:0 caminando, 1 salto ...)
		// {tipo, posicionFijaX, pocicionFijaY,MinimoX, MaximoX, SaltoY, velocidad, tiempoSaltoMS, 0 = bandX, 0 = bandY, 0 = timeCont}
		public int [,] enemyType;

		// Rectangulo y textura de Enemy 
		public Rectangle [] enemyRectangle;
		public Texture2D [] enemySprite;

		public int enemyAnimNumber,enemyCant;
		public string enemyAction;

		public Enemy ()
		{
			enemyAction = "L";
			enemyType = new int[enemyCant,10];
			enemyAnimNumber = 0;
			enemyCant = 0;
		}


		// carga el contenid de enemigo
		public void Load (BoxMan game)
		{
			this.enemySprite = new Texture2D[30];
			// centinellas
			this.enemySprite[0] = game.Content.Load<Texture2D> ("enemy/" + "EnemyL" + "00");
			this.enemySprite[1] = game.Content.Load<Texture2D> ("enemy/" + "EnemyL" + "10");
			this.enemySprite[2] = game.Content.Load<Texture2D> ("enemy/" + "EnemyL" + "20");
			this.enemySprite[3] = game.Content.Load<Texture2D> ("enemy/" + "EnemyL" + "30");
			this.enemySprite[4] = game.Content.Load<Texture2D> ("enemy/" + "EnemyL" + "40");
			// cannon
			this.enemySprite[5] = game.Content.Load<Texture2D> ("enemy/" + "CanonL" + "00");
			this.enemySprite[6] = game.Content.Load<Texture2D> ("enemy/" + "CanonL" + "10");
			this.enemySprite[7] = game.Content.Load<Texture2D> ("enemy/" + "CanonL" + "20");
			this.enemySprite[8] = game.Content.Load<Texture2D> ("enemy/" + "CanonL" + "30");
			this.enemySprite[9] = game.Content.Load<Texture2D> ("enemy/" + "CanonL" + "40");
			// cubi
			this.enemySprite[10] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "00");
			this.enemySprite[11] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "10");
			this.enemySprite[12] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "20");
			this.enemySprite[13] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "30");
			this.enemySprite[14] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "40");
			this.enemySprite[15] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "01");
			this.enemySprite[16] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "11");
			this.enemySprite[17] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "21");
			this.enemySprite[18] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "31");
			this.enemySprite[19] = game.Content.Load<Texture2D> ("enemy/" + "CubiL" + "41");
			this.enemySprite[20] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "00");
			this.enemySprite[21] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "10");
			this.enemySprite[22] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "20");
			this.enemySprite[23] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "30");
			this.enemySprite[24] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "40");
			this.enemySprite[25] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "01");
			this.enemySprite[26] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "11");
			this.enemySprite[27] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "21");
			this.enemySprite[28] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "31");
			this.enemySprite[29] = game.Content.Load<Texture2D> ("enemy/" + "CubiR" + "41");

		}

		/// <summary>
		/// esta funsion se llama dentro de level si se han definido enemigos
		/// </summary>
		public void ReLoad ()
		{
			//carga enemigos si se han definido
			if (enemyCant > 0) {
				this.enemyRectangle = new Rectangle [this.enemyCant];
				for (int i = 0; i < this.enemyCant; i++) {
					// Crea enemigo centinella
					if (enemyType [i, 0] < 5)
						this.enemyRectangle [i] = new Rectangle (enemyType [i, 1], enemyType [i, 2], 60, 30);
					// Crea enemigo cannon
					else if (enemyType [i, 0] >= 5 &&  enemyType [i, 0] <= 9) {
						this.enemyRectangle [i] = new Rectangle (enemyType [i, 1], enemyType [i, 2], 30, 30);
						enemyType [i, 9] = 0; // band
						enemyType [i, 10] = 0; // escribimos 0 para contador de tiempo
					}
					// Crea enemigo cubi
					else {
						this.enemyRectangle [i] = new Rectangle (enemyType [i, 1], enemyType [i, 2], 40, 40);
						enemyType [i, 5] = enemyType [i, 2] - enemyType [i, 5]; //salto a dar
						enemyType [i, 8] = 0; // bandX
						enemyType [i, 9] = 0; // bandY
						enemyType [i, 10] = 0; // escribimos 0 para contador de tiempo
					}
				}
			}
		}

		/// <summary>
		/// Actualiza comportamiento de enemigo
		/// y controla si colisiona con player
		/// <param name="player">Player.</param>
		public int Update(Player player, int gameCont ,int _band = 0)
		{
			int band = _band;
			for (int i=0; i<this.enemyCant; i++)
			{
				// enemigos centinellas (recorren una zona)
				if (enemyType [i, 0] >= 0 && enemyType [i, 0] <= 4) {
					if (enemyRectangle [i].Location.X > enemyType [i, 5] && enemyType [i, 9] == 0)
						enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X - enemyType [i, 3], this.enemyRectangle [i].Location.Y);
					else if (enemyRectangle [i].Location.X < enemyType [i, 6] && enemyType [i, 9] == 1)
						enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X + enemyType [i, 3], this.enemyRectangle [i].Location.Y);
					else {
						if (enemyType [i, 9] == 1)
							enemyType [i, 9] = 0;
						else
							enemyType [i, 9] = 1;
					}
					if (enemyRectangle [i].Location.Y > enemyType [i, 7] && enemyType [i, 10] == 0)
						enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X, this.enemyRectangle [i].Location.Y - enemyType [i, 4]);
					else if (enemyRectangle [i].Location.Y < enemyType [i, 8] && enemyType [i, 10] == 1)
						enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X, this.enemyRectangle [i].Location.Y + enemyType [i, 4]);
					else {
						if (enemyType [i, 10] == 1)
							enemyType [i, 10] = 0;
						else
							enemyType [i, 10] = 1;
					}
				}
				// enemigos "Canon" (salen siempre del mismo lugar, hacen su recorrido y desaparecen por un cierto tiempo, pueden variar su velocidad, tiempo en desaparecer y lanzarse)
				else if (enemyType [i, 0] >= 5 && enemyType [i, 0] <= 9) {

					// es el tiempo de este enemigo mayor o igual al tiempo que debe esperar
					if (enemyType [i, 7] <= enemyType [i, 10]) {
						// movimiento eje X
						if ((enemyRectangle [i].Location.X <= enemyType [i, 5] && enemyType [i, 3] > 0) ||
						    (enemyRectangle [i].Location.X > enemyType [i, 5] && enemyType [i, 3] < 0)) {
							enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X + enemyType [i, 3], this.enemyRectangle [i].Location.Y);
						} else if (enemyType [i, 3] != 0) {
							enemyType [i, 9] += 1;
						}

						// movimiento eje Y
						if ((enemyRectangle [i].Location.Y <= enemyType [i, 6] && enemyType [i, 4] > 0) ||
						    (enemyRectangle [i].Location.Y > enemyType [i, 6] && enemyType [i, 4] < 0)) {
							enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X, this.enemyRectangle [i].Location.Y + enemyType [i, 4]);
						} else if (enemyType [i, 4] != 0) {
							enemyType [i, 9] += 2;
						}

						// si posee movimiento en X e Y y finalizaron ambos o si tiene solo 1 movimiento y finalizo
						if (((enemyType [i, 4] != 0 && enemyType [i, 3] != 0) && enemyType [i, 9] == 3) ||
						    ((enemyType [i, 4] == 0 && enemyType [i, 3] != 0) && enemyType [i, 9] == 1) ||
						    ((enemyType [i, 4] != 0 && enemyType [i, 3] == 0) && enemyType [i, 9] == 2)) {
							enemyType [i, 9] = 0;
							enemyType [i, 10] = 0;
							enemyRectangle [i].Location = new Point (this.enemyType [i, 1], this.enemyType [i, 2]);
						}
					} else {
						enemyType [i, 10] += gameCont;
					}
				}
				// enemigo cubi (annimado) caminan en X y saltan cada cierto tiempo
				else if (enemyType [i, 0] >= 10 && enemyType [i, 0] < 30) {	// en cubi (band 1:0 caminando, 1 salto ...)
					// {tipo, posicionFijaX, pocicionFijaY,MinimoX, MaximoX, SaltoY, velocidad, tiempoSaltoMS, 0 = bandX, 0 = bandY, 0 = timeCont}
					if (enemyType [i, 9] == 1) {
						enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X, this.enemyRectangle [i].Location.Y -7);
						if (enemyRectangle [i].Location.Y <= enemyType [i, 5])
							enemyType [i, 9] = 2;
					}  
					else if (enemyType [i, 9] == 2)
					{
						enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X, this.enemyRectangle [i].Location.Y +7);
						if (enemyRectangle [i].Location.Y >= enemyType [i, 2])
							enemyType [i, 9] = 0;
					}
					else if (enemyType [i, 9] == 0) {
						enemyType [i, 10] += gameCont;
						// cuando se llega al limite de tiempo salta el enemigo
						if (enemyType [i, 10] > enemyType [i, 7]){
							enemyType [i, 10] = 0;
							enemyType [i, 9] = 1; //salta cuando se cumple el tiempo
						}

						if (gameCont % 40 == 0) { //tiempo de nimacion
							if ((enemyType [i, 0] < 15 && enemyType [i, 0] > 9) ||
							    (enemyType [i, 0] < 25 && enemyType [i, 0] > 19))
								enemyType [i, 0] += 5;
							else
								enemyType [i, 0] -= 5; 
						}

						// cambio de posisiones
						if (enemyRectangle [i].Location.X > enemyType [i, 3] && enemyType [i, 8] == 0)
							enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X - enemyType [i, 6], this.enemyRectangle [i].Location.Y);
						else if (enemyRectangle [i].Location.X < enemyType [i, 4] && enemyType [i, 8] == 1)
							enemyRectangle [i].Location = new Point (this.enemyRectangle [i].Location.X + enemyType [i, 6], this.enemyRectangle [i].Location.Y);
						else {
							if (enemyType [i, 8] == 1) {
								enemyType [i, 8] = 0;
								if (enemyType [i, 0] >= 20)
									enemyType [i, 0] -= 10;
							} else {
								enemyType [i, 8] = 1;
								if (enemyType [i, 0] < 20)
									enemyType [i, 0] += 10;
							}

						}
					}
				}

				// si player choca con enemigo (negro o de color segun el caso) pierde!...
				if (player.playerRectangle.Intersects(this.enemyRectangle [i]))
				{
					if (player.playerRectangle.Y + player.playerHeight / 6 <= this.enemyRectangle [i].Location.Y + this.enemyRectangle [i].Height &&
					   (player.playerRectangle.X + (player.playerWidth - player.playerWidth / 6) > this.enemyRectangle [i].Location.X &&
					    player.playerRectangle.X + player.playerWidth / 6 < this.enemyRectangle [i].Location.X + this.enemyRectangle [i].Width) &&
						(enemyType [i,0] == 0 || enemyType[i,0] == player.playerType || enemyType [i,0] == 5 || enemyType[i,0]-5 == player.playerType ||
						 enemyType [i,0] == 10 || enemyType [i,0] == 15 || enemyType [i,0] == 20 || enemyType [i,0] == 25 || enemyType [i,0]-10 == player.playerType ||
						 enemyType [i,0]-15 == player.playerType || enemyType [i,0]-20 == player.playerType || enemyType [i,0]-25 == player.playerType ))
					{
						band = -1;
					}
				}
			}
			return band;
		}

		public void Draw (SpriteBatch spriteBatch, Color gameColor)
		{
			for (int i = 0; i < this.enemyCant; i++) {
				// dibuja la textura actual
				spriteBatch.Draw (this.enemySprite [this.enemyType [i, 0]], this.enemyRectangle [i], gameColor);
			}
		}
	}
}

