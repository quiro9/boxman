﻿using System;

namespace BoxMan
{
	public class Level02
	{
		private Level level;
		private Enemy enemy;

		public Level02 (Level _level, Enemy _enemy)
		{
			level = _level;
			enemy = _enemy;

			// tiempo en nivel
			level.levelTime = 105;

			// limites del nivel
			level.levelMinX = -600; 
			level.levelMaxX = 700;
			level.levelMinY = -500;
			level.levelMaxY = 1450;

			// {tipo, posicionInicialX, pocicionInicialY,desplazamientoX, desplazamientoY, minimoX, MaximoX, minimoY, MaximoY, banderaX, banderaY}
			// {tipo, posicionFijaX, pocicionFijaY,+-desplazamientoX, +-desplazamientoY, valorX, valorY, tiempoMS, null, 0 = band, 0 = timeCont}
			// {tipo, posicionFijaX, pocicionFijaY,MinimoX, MaximoX, SaltoY, velocidad, tiempoSaltoMS, variacion(Rand 0-n), 0 = band, 0 = timeCont}
			enemy.enemyCant = 7;
			enemy.enemyType = new int[ 7, 11] {
				{ 7, 60, 765, 0, -6, -10, -275, 30000,0,0,0 }, // enemigo sale cada 3 seg
				{ 7, 25, 765, 0, -5, 55, -275, 40000,0,0,0 }, // enemigo sale cada 4 seg
				{ 7, -10, 765, 0, -6, 120, -275, 30000,0,0,0 }, // enemigo sale cada 3 seg

				{ 5, -320, 672, 3, -5, 210, -370, 20000,0,0,0 }, // enemigo sale cada 5 seg

				{ 14, 450, 615, 350, 650, 100, 3, 130000,0,0,0 }, // cubi!
				{ 12, 650, 615, 320, 550, 100, 2, 80000,0,0,0 }, // cubi!
				{ 14, -50, 1295, -150, 350, 100, 2, 90000,0,0,0 } // cubi!


			};

			// {tipo, positionX, PositionY, eje, min, max,  velocidad, Widhth, Height, band };
			// tipo: dibuja rectangulo del esenario o objetos;
			// tipo de 0 a 4 rectangulos: 0 negro, 1 azul, 2verde, 3rojo 4 amarillo.
			// de 5 a 9 pinceles: 5 negro, 6 azul, 7 verde, 8 rojo 9 amarillo.
			// de 10 a 29 pinchos: 10 negro, 11 azul, 12 verde, 13rojo, 14 amarillo ...
			// de 30 a 34 monedas ... ( todo posee el mismo orden de colores C/5 valores)
			// de 35 a 39 llaves, 40 a 44 bloques llaves view, 45 a 49 bloques llaves hide
			// de 50 a 54 bandera "gano" ... 55 es un bloque del color del fondo puede ocultar objetos,
			// para cosas raras como aparecer objetos o enemigos usar 56 que es transparente
			// eje: indica movimiento 1 ejeX, 2 EjeY
			// min, max: indica valor minimo y maximo en el eje para que s emueva
			// velocidad: cantidad de pixeles
			//    para objetos estaticos colocar valores minX, maxX, minY,maxY, velocidad en 0
			// band: varia segun el tipo, en plataformas mobiles indica "arriba abajo" o "derha izquierda"
			level.rectangleCant = 64;
			level.rectangleType = new int[ 64, 10] {

				// bloques amarillos
				{4,210,470, 0,0,0,0, 50,50 ,0},

				// zona caida amarilla
				{4,-150,1330, 0,0,0,0, 750,50 ,0},
				{4,-300,1520, 0,0,0,0, 900,100 ,0},
				{4, 750,1520, 1,550,1600,3, 80,50 ,0},
				{4, 550, 800, 0, 50, 650, 0, 50,530,0 },

				{9,-60,1150, 0,0,0,0, 50,50 ,0}, // pincel amarillo

				{34,-15,230, 0,0,0,0, 20,30 ,0}, //monedas amarillas
				{34,35,230, 0,0,0,0, 20,30 ,0},
				{34,460,1260, 0,0,0,0, 20,30 ,0},
				{34,460,1210, 0,0,0,0, 20,30 ,0},
				{34,500,1260, 0,0,0,0, 20,30 ,0},
				{34,500,1210, 0,0,0,0, 20,30 ,0},


				//bloques negros
				{0,-640,650, 0,0,0,0, 500,150 ,0}, 
				{0,-40,650, 0,0,0,0, 750,150 ,0},

				{0,-90,300, 0,0,0,0, 200,50 ,0},
				{0,160,420, 0, 0, 0, 0,50,50,0 },

				{0,-95,600, 0,-101,701,2, 50,50 ,1},
				{45,-95,550, 0,-101,701,2, 50,50 ,1}, //se mueve!

				{35,-270,500, 0,0,0,0, 20,40 ,1}, 	// llave negra
				{45,-590,550, 0, 0,0,0, 100,100 ,0}, //tapa bandera!
				{45,-30,200, 0, 0,0,0, 100,100 ,0}, //tapa monedas verdes!

				{40,-140,650, 0, 0, 0, 0,100,100,0 },


				//bloques rojos
				{8,-200, -20, 0,0,0,0, 50,50 ,0}, //pincel rojo

				{33,-190,150, 0,0,0,0, 20,30 ,0}, // monedas rojas
				{33,-230,150, 0,0,0,0, 20,30 ,0},
				{33,20,550, 0,0,0,0, 20,30 ,0},
				{33,70,550, 0,0,0,0, 20,30 ,0},

				// zona caida roja
				{3,-50,960, 0,0,0,0, 350,50 ,0},
				{3,-140,750, 0,0,0,0, 50,50 ,0},
				{3, 200,1100, 0,0,0,0, 100,50 ,0},
				{3,-150,1250, 0,0,0,0, 300,80 ,0},

				{33,0,900, 0,0,0,0, 20,30 ,0}, // mas monedas rojas
				{33,50,900, 0,0,0,0, 20,30 ,0}, 
				{33,100,900, 0,0,0,0, 20,30 ,0}, 
				{33,150,900, 0,0,0,0, 20,30 ,0}, 
				{33,200,900, 0,0,0,0, 20,30 ,0}, 
				{33,250,900, 0,0,0,0, 20,30 ,0},

				{33,210,1020, 0,0,0,0, 20,30 ,0}, 
				{33,260,1020, 0,0,0,0, 20,30 ,0}, 
				{33,210,1060, 0,0,0,0, 20,30 ,0}, 
				{33,260,1060, 0,0,0,0, 20,30 ,0}, 

				// bloques verdes
				{2,-490,400, 0,0,0,0, 50,250 ,0}, 
				{2,110,470, 0,0,0,0, 50,50 ,0},

				{47,-240,200, 0,0,0,0, 50,50 ,0}, 
				{47,-290,150, 0,0,0,0, 50,50 ,0}, 

				{37,-190,500, 0,0,0,0, 20,40 ,1}, // llave verde
				{7,110,150, 0,0,0,0, 50,50 ,0}, //pincel verde
				{52,-550,580, 0, 0, 0, 0, 40,70, 0},	// llegada verde

				// zona caida negra
				{ 0,-50, 800, 0,-650,-50, 1, 50,600,0 }, //pilares se mueven
				{25,  0, 800, 0,-600,  0, 1, 25,100,0 },
				{25,  0, 900, 0,-600,  0, 1, 25,100,0 },
				{25,  0,1000, 0,-600,  0, 1, 25,100,0 },
				{25,  0,1100, 0,-600,  0, 1, 25,100,0 },
				{25,  0,1200, 0,-600,  0, 1, 25,100,0 },
				{25,  0,1300, 0,-600,  0, 1, 25,100,0 },

				{ 0, 50, 800, 0, 50, 650, 1, 50,600,0 },
				{20, 25, 800, 0, 25, 625, 1, 25,100,0 },
				{20, 25, 900, 0, 25, 625, 1, 25,100,0 },
				{20, 25,1000, 0, 25, 625, 1, 25,100,0 },
				{20, 25,1100, 0, 25, 625, 1, 25,100,0 },
				{20, 25,1200, 0, 25, 625, 1, 25,100,0 },
				{20, 25,1300, 0, 25, 625, 1, 25,100,0 },

				{35, 300,1250, 0, 0, 0, 0, 20,40,0 }, //lave negra

				{0,-290,1100, 0,0,0,0, 350,50 ,0}
			};
		}

	}
}

