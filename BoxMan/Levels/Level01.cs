﻿using System;

namespace BoxMan
{
	public class Level01
	{
		private Level level;
		private Enemy enemy;

		public Level01 (Level _level, Enemy _enemy)
		{
			level = _level;
			enemy = _enemy;

			// tiempo en nivel
			level.levelTime = 60;

			// limites del nivel
			level.levelMinX = -200; 
			level.levelMaxX = 700;
			level.levelMinY = -100;
			level.levelMaxY = 850;

			Random ran;
			ran = new Random ();
			int banderaX, banderaY, posicionInicialX, pocicionInicialY, desplazamientoX, desplazamientoY ;
			banderaX = ran.Next (0,1);
			banderaY = ran.Next (0,1);
			posicionInicialX = ran.Next (-350, 120);
			pocicionInicialY = ran.Next (100, 620);
			desplazamientoX = ran.Next (4, 5);
			desplazamientoY = ran.Next (2, 3);

			// {tipo, posicionInicialX, pocicionInicialY,desplazamientoX, desplazamiento Y, minimoX, MaximoX, minimoY, MaximoY, banderaX, banderaY}
			enemy.enemyCant = 3;
			enemy.enemyType = new int[ 3, 11] {
				{ 0, posicionInicialX, pocicionInicialY, desplazamientoX, desplazamientoY, -360, 130, 50, 630,banderaX,banderaY },
				{ 1, 420, 465, 0, 4, 0, 0, 360, 600,0,1 },
				{ 2, -250, 720, 2, 0, -430, -195,0 ,0 ,banderaX,1 }

			};
			pocicionInicialY = ran.Next (600, 950);


			// {tipo, positionX, PositionY, eje, min, max,  velocidad, Widhth, Height, band };
			// tipo: dibuja rectangulo del esenario o objetos;
			// tipo de 0 a 4 rectangulos: 0 negro, 1 azul, 2verde, 3rojo 4 amarillo.
			// de 5 a 9 pinceles: 5 negro, 6 azul, 7 verde, 8 rojo 9 amarillo.
			// de 10 a 29 pinchos: 10 negro, 11 azul, 12 verde, 13rojo, 14 amarillo ...
			// de 30 a 34 monedas ... ( todo posee el mismo orden de colores C/5 valores)
			// de 35 a 39 llaves, 40 a 44 bloques llaves view, 45 a 49 bloques llaves hide
			// de 50 a 54 bandera "gano" ... 55 es un bloque del color del fondo puede ocultar objetos,
			// para cosas raras como aparecer objetos o enemigos usar 56 que es transparente
			// eje: indica movimiento 1 ejeX, 2 EjeY
			// min, max: indica valor minimo y maximo en el eje para que s emueva
			// velocidad: cantidad de pixeles
			//    para objetos estaticos colocar valores minX, maxX, minY,maxY, velocidad en 0
			// band: varia segun el tipo, en plataformas mobiles indica "arriba abajo" o "derha izquierda"
			level.rectangleCant = 33;
			level.rectangleType = new int[ 33, 10] {
				//bloques azul
				{1,200,250, 0,0,0,0, 50,400 ,0},
				{1,320,300, 1,250,490,3, 80,50 ,0},
				{1,850,600, 1,530,950,2, 50,50 ,banderaX},
				{31,860,750, 0,0,0,0, 20,30 ,1},
				{31,860,825, 0,0,0,0, 20,30 ,1},
				{31,860,900, 0,0,0,0, 20,30 ,1},

				{6,269,420, 0,0,0,0, 50,50 ,0}, //pincel Azul


				//bloques verde
				{2,-350,pocicionInicialY, 1,600,950,2, 80,50 ,banderaY},
				{2,-100,400, 1,0,0,0, 50,250 ,0},
				{2,-50,525, 1,0,0,0, 50,125 ,0},

				{32,20,550, 0,0,0,0, 20,30 ,0}, //monedas verdes
				{32,70,550, 0,0,0,0, 20,30 ,0},
				{32,-308,760, 0,0,0,0, 20,30 ,0},
				{32,-308,810, 0,0,0,0, 20,30 ,0},

				{7,-255, -20, 0,0,0,0, 50,50 ,0}, //pincel Verde

				// bloques roja
				{3,-300,200, 0,0,0,0, 250,50 ,0},


				{33,-250,150, 0,0,0,0, 20,30 ,0}, // monedas rojas
				{33,-200,150, 0,0,0,0, 20,30 ,0},
				{33,-150,150, 0,0,0,0, 20,30 ,0},

				{8,100,150, 0,0,0,0, 50,50 ,0}, //pincel rojo


				//bloques negra
				{0,-150,650, 0,0,0,0, 900,150 ,0}, 
				{0,500,550, 0,0,0,0, 300,100 ,0},
				{0,100,450, 1,250,400,2, 50,50 ,1},
				{30,-85,550, 0,0,0,0, 20,30 ,1}, // momedas negras
				{30,-85,500, 0,0,0,0, 20,30 ,1},

				// llegada verde
				{12,-100,1000, 0, 0, 0, 0,60,20,0 }, // espina
				{12,120,1000, 0, 0, 0, 0,60,20,0 }, // espina

				{2,-230,1020, 0, 0, 0, 0,550,50,0 },
				{52, 40,950, 0, 0, 0, 0, 40,70, 0},

				{32,200,950, 0,0,0,0, 20,30 ,0}, //monedas verdes
				{32,200,900, 0,0,0,0, 20,30 ,0},
				{32,250,950, 0,0,0,0, 20,30 ,0},
				{32,250,900, 0,0,0,0, 20,30 ,0}

			};
		}

	}
}

