﻿using System;

namespace BoxMan
{
	/// <summary>
	///  esenario "default" (final)
	/// </summary>
	public class NoFinal
	{
		private Level level;

		public NoFinal (Level _level, Enemy enemy)
		{
			level = _level;


			// tiempo de nivel
			level.levelTime = 999999999;

			// limite de nivel
			level.levelMinX = -500; 
			level.levelMaxX = 800;
			level.levelMinY = 100;
			level.levelMaxY = 700;


			// {tipo, posicionInicialX, pocicionInicialY,desplazamientoX, desplazamiento Y, minimoX, MaximoX, minimoY, MaximoY, banderaX, banderaY}
			enemy.enemyCant = 4;
			enemy.enemyType = new int[ 4, 11] {
				{ 1, 30, 400, 3, 5, -455, 650, 260, 730, 1, 0 },
				{ 2, 0, 465, 4, 3, -450, 650, 280, 740, 0, 1 },
				{ 3, 20, 620, 5, 3, -450, 655, 220, 735, 0, 0 },
				{ 4, 10, 520, 5, 4, -455, 650, 260, 730, 1, 1 }

			};

			// {tipo, positionX, PositionY, eje, min, max,  velocidad, Widhth, Height, band };
			// tipo: dibuja rectangulo del esenario o objetos;
			// tipo de 0 a 4 rectangulos: 0 negro, 1 azul, 2verde, 3rojo 4 amarillo.
			// de 5 a 9 pinceles: 5 negro, 6 azul, 7 verde, 8 rojo 9 amarillo.
			// de 10 a 29 pinchos: 10 negro, 11 azul, 12 verde, 13rojo, 14 amarillo ...
			// de 30 a 34 monedas y 35 a 39 bandera "gano" ... (mismo orden)
			// eje: indica movimiento 1 ejeX, 2 EjeY
			// min, max: indica valor minimo y maximo en el eje para que s emueva
			// velocidad: cantidad de pixeles
			// para objetos estaticos colocar valores minX, maxX, minY,maxY, velocidad en 0
			level.rectangleCant = 3;
			level.rectangleType = new int[ 3, 10] {

				// zona negra
				{0,-500,750, 0, 0, 0, 0,1250,250, 0},
				{0,-500,250, 0, 0, 0, 0,50,500, 0},
				{0,700,250, 0, 0, 0, 0,50,500, 0}

			};

		}

	}
}

