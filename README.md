﻿# BoxMan - "FINALIZADO"
 
Boxman creado por Juan Quiroga (quiro90), Se licencia bajo GNU/GPL v3.

_______
Boxman es un juego de plataformas básico, consiste en un "personaje" cuadrado que avanza nivel a nivel tomando monedas, esquivando enemigo, recorriendo la pantalla y tomando pinceles para avanzar...

Esta escrito en MonoDevelop (lenguaje C#) y utiliza librerías Monogame, el código fue escrito desde linux; así que al querer ejecutar en windows puede llegar a tener que adaptar alguna parte.

( Recientemente se que el sonido no anda en windows con openAL (si al momento de ejecutar pero da problemas para compilar), por lo tanto debe adaptar el formato o bien sacar las lineas de sonido:
Las especializaciones están al inicio de "level.cs", y luego busca las reproducciones de audio a lo largo de la librería como .play (); y la única reproducción que no esta en "level.cs" se encuentra en "playerkey.cs" )

Este es un proyecto de Clases a el cual le estoy dedicando tiempo necesario, pero una vez concretado a futuro; no creo dedicarle tiempo alguno.

Tenga en cuenta que es un primer juego y realizado en poco tiempo (unas 4 semanas), con librerías que si bien son para realizar juegos suele complementarse con otros frameworks "mas completos" que no utilice (dado que solo debía usar solo monogame):

_____
Por estos motivo advierto:
* El código esta un poco desprolijo, no espere ver todo acomodado.
* Los controles son un desastre, no utilice principios físicos para ningún objeto, simplemente se mueven por píxeles y lógica.
* He "aprendido POO" a medida que programe estas 3 semanas,por lo tanto verá cosas extrañas por falta de practica.
* El razonamiento "apurado" evidentemente puede dejar algunos errores, pueden suceder fallos inesperadamente.
* El juego pudo haberse pulido muchísimo y aun con lo echo ampliarse un poco más, no piense que es la única forma de realizar un juego plataformas.

De todas formas desde ahora hasta que lo trabaje, dejaré este código disponible, no es muy extenso y si requiere verlo como un ejemplo puede serle útil!. Gracias por leer y descargar!.
